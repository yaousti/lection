---
title: "Control de Costos"
slug: "control_de_costos"
description : ""
img: "service.png"
---
 ## Control de Costos

- **Resolució de problemes concrets.** *resolem problemes concrets en el producte*
- **Ajustos de productivitat.** *crear un sistema que coordini la productivitat amb la venda i avaratir costos i millorar productivitat*
- **Control del pressupost** *realitzem totes les previsions i a més proposem els reajustaments que s'han de fer*
- **Ajustament de les previsions**  **Controller**.  *controlam si lo que s'esta fent ve d'acord mb lo que s'havia estipulat a les previsions*
- **Control d’entrada/sortida d’estocs i el seu malbaratament.**  *preparar un plan de control de estoc que suposa un abaratiment en el manteniment i que intenti perdre el mínim de les mercaderies o que es fagin malbé*
- **Anàlisis de costos relacionats amb la producció.**  *analitzam els seus costos i la manera de reduir-los i sopesem manera de producció mes eficients amb els recursos*

