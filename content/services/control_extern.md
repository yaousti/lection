---
title: "Control Extern"
slug: "control_extern"
description : ""
img: "service.png"
---

## Control Extern

- **Recerca de proveïdors i subministradors** *aprofitar la nostra experiencia per contractatar als millors proveidors i subministradors*
- **Sistema de gestió** *mirar quin estil de gestió afavoreix a la seva empresa i implantar-lo*
- **Estructura financera** *crear un estructura on es paga'n menys interessos o com a mínim es paga'n amb mes facilitat,com també podem intentar buscar la millor manera de financiacio de l' empresa.*
- **Imatge corporativa** *es procurara amb una iniciativa de màrqueting crear una imatja molt positiva dels seus grups d'interès sobre l'empresa*
- **Responsabilitat social corporativa** *es realitzaran estudis per fer una inversió en RSC que proporcioni la major repercussió en els seus grups d'interès*
- **Màrqueting mixt** *Preu, producte, distribució i publicitat* *recerca de la millor conversió de les 4 variables*
- **Comunicació amb els stakeholders** *tractament amb els grups d’interès* 