---
title: "Control Intern"
slug: "control_intern"
description : ""
img: "service.png"
---
## Control Intern
- **Camí de factures i burocràcia interna** *creació de protocols amb factures i burocràcia per millorar la direcció i la administració de l' empresa ,fent aixi que no es perdi temps i diners en el mal control intern*
- **Assessorament fiscal, laboral i de processos.** *ajuda en la presa de decisions de caire fiscal, laboral i de processos.*
- **Calendari estructural de l’empresa.(Sistema PERT-CPM)** *calendari que ajuda a programar els dies importants de l'empresa on te que remetre obligacions tant tributaries com de altre caire que ajuda a la presa de decisions i facilita la direcció i la administració*
- **Sistema de pressupostos** *generem una previsió de pressupostos i la manera de dur-los a terme i revisions periòdiques amb les corresponent ideàs per la correcció*
- **Control d’estocs** *creació de sistema que permeti el control de estocs i ajuda a la seva implantació i explicació a tot el personal*
- **Realització de previsions de balanços, pèrdues i guanys** *a un any vista* *una previsió que ens permetrà simular com podrà ser la nostra empresa i poder prendre decisions amb mes facilitat i també facilitem les mesures reguladores per intentar aconseguir els objectius*
- **Control de tresoreria** *un control que li permetrà fer front els pagos i aixi estalviar-se els diners per recarecs i interessos*
- **QDI. Quadre de comandament integral** *TO DEIXO A TU NIL JEJEJE*
