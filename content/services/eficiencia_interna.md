---
title: "Eficiencia Interna"
slug: "eficiencia_interna"
description : ""
img: "service.png"
---
## Eficiencia Interna

- **Anàlisi d’eficiència dels processos interns** *intentar dissenyar sistemes de processos mes eficients*
- **Organització del personal** *reestructuració del organigrama i el seu seguiment per solucionar la adversió al risc*
- **Reassignació de les tasques en funció de les aptituds** *intentar millorar el rendiment del personal amb ajudes tant psicològiques com de recerca de millora de l'eficiència en el seu lloc de feina com negociacions de sous i jornades*
- **Sistema de ERP** *sistema de planificació de recursos empresarials*
- **Creació de protocols de funcionament** *crear protocols que fagin millorar la eficiència en la presa de decisions i aixi facilitar la direcció de l empresa*
- **Responsabilitat social corporativa** *realitzem tan grans o petits events on publicitem l'empresa mitjançant programes de RSC*